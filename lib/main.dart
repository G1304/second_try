import 'package:flutter/material.dart';
import 'package:second_try/pages/main_menu.dart';
import 'package:second_try/pages/comments/fetchcomments.dart';
import 'package:second_try/pages/notes/notes.dart';
import 'package:second_try/pages/photos/fetchphotos.dart';
import 'package:second_try/pages/todos/todos.dart';


import 'package:second_try/pages/notes_test/notestest.dart';


void main() => runApp(MaterialApp(
  theme: ThemeData(
    primaryColor:Colors.grey[700],
  ),
  initialRoute: '/',
  routes: {
    '/': (context) => MainMenu(),
    '/todos' : (context) => Todos(),
    '/fetchcomments' : (context) => FetchComments(),
    '/fetchphotos' : (context) => FetchPhotos(),
    '/notes' : (context) => Notes(),
    '/notestest' : (context) => NotesTest(),
  },
));
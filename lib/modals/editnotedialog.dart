import 'package:flutter/material.dart';
import 'package:second_try/pages/notes/notes.dart';

class EditNoteDialog extends StatefulWidget {
  const EditNoteDialog(
      {Key? key,
      required this.note,
      required this.notesList,
      required this.callback})
      : super(key: key);
  final Note note;
  final List<Note> notesList;
  final Function callback;

  @override
  _EditNoteDialogState createState() => _EditNoteDialogState();
}

class _EditNoteDialogState extends State<EditNoteDialog> {
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: TextFormField(
                    initialValue: widget.note.title,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Title',
                      isDense: true,
                      fillColor: Colors.grey[700],
                    ),
                    onChanged: (String value) {
                      widget.note.title = value;
                    }),
              ),
              TextFormField(
                  initialValue: widget.note.note,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Description",
                    fillColor: Colors.grey[700],
                  ),
                  maxLines: 12,
                  minLines: 8,
                  onChanged: (String value) {
                    widget.note.note = value;
                  }),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: OutlinedButton(
              onPressed: () {
                widget.note.title = widget.note.title;
                widget.note.note = widget.note.note;

                widget.notesList.singleWhere((item) =>
                item.id == widget.note.id).note = widget.note.note;

                widget.notesList.singleWhere((item) =>
                item.id == widget.note.id).title = widget.note.title;

                widget.callback(widget.notesList);
                Navigator.of(context).pop();
              },
              child: Text("Edit Note")),
        ),
      ],
    );
  }
}

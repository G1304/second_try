import 'package:flutter/material.dart';

class _DefAppBarState extends State<DefAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.red[700],
      title: Text(widget.title),
      centerTitle: true,
    );
  }
}

class DefAppBar extends StatefulWidget implements PreferredSizeWidget {
  final String title;

  DefAppBar(this.title,
      {Key ? key,}) : super(key: key);

  @override
  _DefAppBarState createState() => _DefAppBarState();

  @override
  Size get preferredSize => Size.fromHeight(kToolbarHeight);
}

import 'package:flutter/material.dart';
import 'package:second_try/pages/notes/notes.dart';

class NoteDetailed extends StatefulWidget {
  const NoteDetailed({Key? key, required this.note,
    required this.notesList,
  required this.callback}) : super(key: key);
  final Note note;
  final List<Note> notesList;
  final Function callback;

  @override
  _NoteDetailedState createState() => _NoteDetailedState();
}

class _NoteDetailedState extends State<NoteDetailed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[200],
      appBar: AppBar(
        backgroundColor: Colors.blueGrey[900],
        title: Text(widget.note.title),
      ),
      body: SafeArea(
        child: Column(
          children: [
            Align(
              alignment: Alignment.topRight,
              widthFactor: 8.0,
              child: Padding(
                padding: const EdgeInsets.only(top: 2.0, right: 5.0),
                child: Text(
                  widget.note.fulldate,
                  textAlign: TextAlign.end,
                  style: TextStyle(color: Colors.grey[700]),
                ),
              ),
            ),
            Expanded(flex: 1,
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Align(
                      alignment: Alignment.topLeft,
                        child: Text(
                          widget.note.note,
                          style: TextStyle(
                              height: 1.3,
                              fontSize: 18,
                              fontWeight: FontWeight.w400,
                              color: Colors.grey[90]
                          ),
                        ),
                    )
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.delete),
        onPressed: () {
          widget.notesList.removeWhere((item) => item.id == widget.note.id);
          Navigator.of(context).pop();
          widget.callback(widget.notesList);
        },
      ),
    );
  }
}

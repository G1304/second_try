import 'package:flutter/material.dart';
import 'package:second_try/modals/editnotedialog.dart';
import 'package:second_try/pages/notes/note_detailed.dart';
import 'package:second_try/widgets/appbar/appbar.dart';

class Note {
  int id;
  String title;
  String note;
  String date;
  String fulldate;

  Note({
    required this.id,
    required this.title,
    required this.note,
    required this.date,
    required this.fulldate,
  });
}

class Notes extends StatefulWidget {
  const Notes({Key? key}) : super(key: key);

  @override
  _NotesState createState() => _NotesState();
}

class _NotesState extends State<Notes> {
  List<Note> notesList = [];
  String title = '', note = '';

  reloadNotesList(newNotesList) {
    setState(() {
      notesList = newNotesList;
    });
  }

  @override
  Widget build(BuildContext context) {
    DateTime today = new DateTime.now();
    String datetime = "${today.year.toString()}-"
        "${today.month.toString().padLeft(2, '0')}-"
        "${today.day.toString().padLeft(2, '0')}";
    String fullDateTime = "${datetime} ${today.hour}:"
        "${today.minute}:"
        "${today.second}";
    _addNote() {
      if (title.isEmpty) title = datetime;
      setState(() {
        notesList.add(Note(
          id: notesList.length + 1,
          title: title,
          note: note,
          date: datetime,
          fulldate: fullDateTime,
        ));
      });
      title = note = '';
      Navigator.of(context).pop();
    }

    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: DefAppBar("Notes"),
      body: GridView.builder(
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2),
          itemCount: notesList.length,
          itemBuilder: (BuildContext context, index) {
            return Padding(
                padding: const EdgeInsets.all(4.0),
                child: GestureDetector(
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding:
                                const EdgeInsets.only(bottom: 8.0, left: 4.0),
                            child: Text(
                              notesList[index].title,
                              style: TextStyle(
                                color: Colors.grey[600],
                                fontSize: 15,
                              ),
                            ),
                          ),
                          Text(
                            notesList[index].note,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 5,
                            style: TextStyle(
                              color: Colors.grey[800],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    print(notesList[index].id);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => NoteDetailed(
                                note: notesList[index],
                                notesList: notesList,
                                callback: reloadNotesList)));
                  },
                  onLongPress: () {
                    showModalBottomSheet(
                        context: context,
                        builder: (context) {
                          return Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              ListTile(
                                leading: Icon(Icons.delete),
                                title: Text("Delete"),
                                onTap: () {
                                  setState(() {
                                    notesList.removeAt(index);
                                  });
                                  Navigator.pop(context);
                                },
                              ),
                              ListTile(
                                leading: Icon(Icons.edit),
                                title: Text("Edit"),
                                onTap: () {
                                  Navigator.of(context).pop();
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return EditNoteDialog(
                                          note: notesList[index],
                                          notesList: notesList,
                                          callback: reloadNotesList,
                                        );
                                      });
                                },
                              )
                            ],
                          );
                        });
                  },
                ));
          }),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Colors.green[500],
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 8.0),
                            child: TextField(
                                decoration: InputDecoration(
                                  border: OutlineInputBorder(),
                                  labelText: 'Title',
                                  isDense: true,
                                  fillColor: Colors.grey[700],
                                ),
                                onChanged: (String value) {
                                  title = value;
                                }),
                          ),
                          TextFormField(
                              initialValue: "",
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Description",
                                fillColor: Colors.grey[700],
                              ),
                              maxLines: 12,
                              minLines: 8,
                              onChanged: (String value) {
                                note = value;
                              }),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: OutlinedButton(
                          onPressed: _addNote, child: Text("Add note")),
                    ),
                  ],
                );
              });
        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:second_try/widgets/appbar/appbar.dart';
import 'package:second_try/pages/todos/todos.dart';

class TodoDetailed extends StatelessWidget {
  const TodoDetailed({Key? key, required this.todo}) : super(key: key);
  final Todo todo;

  @override
  Widget build(BuildContext context) {
    // final todo = Todo.fromJson(ModalRoute.of(context)!.settings.arguments);

    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: DefAppBar("${todo.title}"),
      body: SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
                child: Text(
              "${todo.description}",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.white,
                fontSize: 23,
              ),
            ))
          ],
        ),
      ),
    );
  }
}

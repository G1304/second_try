import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'package:second_try/widgets/appbar/appbar.dart';
import 'package:second_try/pages/todos/todo_detailed.dart';


//todos class
class Todo {
  int id;
  String title;
  String description;

  Todo({
    required this.id,
    required this.title,
    required this.description,
  });
}
//todos class

class Todos extends StatefulWidget {
  const Todos({Key? key}) : super(key: key);

  @override
  _TodosState createState() => _TodosState();
}

class _TodosState extends State<Todos> {
  List<Todo> todoList = [];
  String title = '',
      description = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: DefAppBar("TODO LIST"),
      body: SafeArea(
        child: ListView.builder(
          itemBuilder: (BuildContext context, int index) {
            return Dismissible(
              key: Key(todoList[index].title),
              child: Card(
                child: ListTile(
                  title: Text(todoList[index].title),
                  trailing: IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: () {
                      setState(() {
                        todoList.removeAt(index);
                      });
                    },
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                TodoDetailed(todo: todoList[index])
                        ));
                  },
                ),
              ),
              onDismissed: (direction) {
                todoList.removeAt(index);
              },
            );
          },
          itemCount: todoList.length,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green,
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Center(child: Text("Add Todo")),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextField(
                        onChanged: (String value) {
                          title = value;
                        },
                      ),
                      TextField(
                        onChanged: (String value) {
                          if (description == '') {
                            description = 'Description: None';
                          } else {
                            description = value;
                          }
                        },
                      ),
                      ElevatedButton(
                          onPressed: () {
                            setState(() {
                              if (title != '') {
                                todoList.add(Todo(id: todoList.length + 1,
                                    title: title,
                                    description: description));
                                title = description = '';
                              } else {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(const SnackBar(
                                  content: Text(
                                    "PLEASE ENTER TODO",
                                    textAlign: TextAlign.center,
                                  ),
                                ));
                              }
                              Navigator.of(context).pop();
                            });
                          },
                          child: Text("Add"))
                    ],
                  ),
                );
              });
        },
        child: Icon(Icons.add),
      ),
    );
  }
}

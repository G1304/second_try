import 'package:flutter/foundation.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:second_try/widgets/appbar/appbar.dart';
import 'package:second_try/pages/comments/comment_detailed.dart';

import 'package:second_try/services/api.dart';

class Comment {
  final int postId;
  final int id;
  final String name;
  final String email;
  final String body;

  Comment(
      {required this.postId,
        required this.id,
        required this.name,
        required this.email,
        required this.body});

  factory Comment.fromJson(Map<String, dynamic> json) {
    return Comment(
      postId: json['postId'],
      id: json['id'],
      name: json['name'],
      email: json['email'],
      body: json['body'],
    );
  }
}

class FetchComments extends StatefulWidget {
  const FetchComments({Key? key}) : super(key: key);

  @override
  _FetchCommentsState createState() => _FetchCommentsState();
}

class _FetchCommentsState extends State<FetchComments> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: DefAppBar("Fetched info..."),
      body: SafeArea(
          child: FutureBuilder<List<Comment>>(
              future: fetchComments(http.Client()),
              builder: (context, snapshot) {
                if (snapshot.hasError) {
                  return const Center(
                    child: Text('An error has occured..'),
                  );
                } else if (snapshot.hasData) {
                  return CommentsList(comments: snapshot.data!);
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              })),
    );
  }
}

class CommentsList extends StatelessWidget {
  const CommentsList({Key? key, required this.comments}) : super(key: key);
  final List<Comment> comments;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, index) {
          return GestureDetector(
            child: GridTile(
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    "${comments[index].name}",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CommentDetailed(comment: comments[index])
                )
              );
            },
          );
        });
  }
}

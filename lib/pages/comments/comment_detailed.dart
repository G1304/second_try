import 'package:flutter/material.dart';
import 'package:second_try/pages/comments/fetchcomments.dart';
import 'package:second_try/widgets/appbar/appbar.dart';

class CommentDetailed extends StatelessWidget {
  const CommentDetailed({Key? key, required this.comment}) : super(key: key);

  final Comment comment;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[800],
        appBar: DefAppBar("${comment.name}"),
        body: SafeArea(
          child: Center(
            child: Text(
              "Comment Body:\n \n${comment.body}\n"
                  "\nComment ID:${comment.id}"
                  "\n\nComment Email: \n${comment.email}",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20,
                color: Colors.white,
              ),
            ),
          ),
        ));
  }
}

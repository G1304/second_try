import "package:flutter/material.dart";
import 'package:second_try/pages/notes_test/notestest.dart';
import 'package:second_try/widgets/appbar/appbar.dart';


class TestNoteDetailed extends StatelessWidget {
  const TestNoteDetailed({Key? key,required this.note,
    required this.notesList,
  required this.updateList}) : super(key: key);
  final TestNote note;
  final List<TestNote> notesList;
  final Function updateList;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: DefAppBar(note.title),
      body: Text(note.description),
      floatingActionButton: FloatingActionButton(onPressed: (){
        notesList.removeWhere((element) => element.id == note.id);
        updateList(notesList);
      },
      child: Icon(Icons.delete)),
    );
  }
}

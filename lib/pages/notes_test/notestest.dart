import 'package:flutter/material.dart';
import 'package:second_try/pages/notes_test/notestestdetailed.dart';
import 'package:second_try/widgets/appbar/appbar.dart';

class TestNote {
  int id;
  String title;
  String description;
  String time;

  TestNote({
    required this.title,
    required this.description,
    required this.time,
    required this.id,
  });
}

class NotesTest extends StatefulWidget {
  const NotesTest({Key? key}) : super(key: key);

  @override
  _NotesTestState createState() => _NotesTestState();
}

class _NotesTestState extends State<NotesTest> {
  List<TestNote> notesList = [];
  String title = '', description = '';
  var time = new DateTime.now();


  _updateNotesList(newNotesList){
    setState(() {
      notesList = newNotesList;
    });
    Navigator.of(context).pop();
  }
  _addNote(){
    if(title == '') title = time.toString();
    setState(() {
      notesList.add(TestNote(
          title: title,
          description: description,
          time: time.toString(),
          id: notesList.length + 1));
    });
    title = description = '';
    Navigator.of(context).pop();
  }

  _deleteNote(index){
    setState(() {
      notesList.removeWhere((element) => element.id == notesList[index].id);
    });
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: DefAppBar("title"),
      body: SafeArea(
        child: GridView.builder(
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemCount: notesList.length,
            itemBuilder: (BuildContext context, index) {
              return GridTile(
                  child: GestureDetector(
                child: Card(
                  child: Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top:8.0,bottom: 8.0),
                            child: Text(notesList[index].title,textAlign: TextAlign.start,),
                          ),
                          Text(notesList[index].description, overflow: TextOverflow.ellipsis,maxLines: 9,),
                        ],
                      ),
                    ),
                  )
                ),
                    onLongPress: (){
                  showModalBottomSheet(context: context, builder: (BuildContext context){
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        ListTile(title: Text("Delete"),
                        trailing: Icon(Icons.delete),
                          onTap:() => _deleteNote(index),
                        ),
                        ListTile(title: Text("Edit"),
                          trailing: Icon(Icons.edit),
                          onTap:(){
                          Navigator.of(context).pop();
                          showDialog(context: context, builder: (BuildContext context){
                            return EditDialog(notesList: notesList,
                              note: notesList[index],
                            updateList: _updateNotesList,);
                          });
                          },
                        ),
                      ],
                    );
                  });
                    },
                    onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => TestNoteDetailed(note: notesList[index],
                    notesList: notesList,
                    updateList:_updateNotesList)
                  ));
                    },
              ));
            }),
      ),

      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return SimpleDialog(
                  title: Center(child: Text("Add Dialog")),
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          TextFormField(
                            initialValue: "",
                            decoration: InputDecoration(
                                labelText: "Title",
                                isDense: true,
                                border: OutlineInputBorder()),
                            onChanged: (String titleValue){
                              title = titleValue;
                            },
                          ),
                          Padding(padding: EdgeInsets.all(10)),
                          TextFormField(
                            initialValue: "",
                            minLines: 4,
                            maxLines: 7,
                            decoration: InputDecoration(
                                labelText: "Description",
                                isDense: true,
                                border: OutlineInputBorder()),
                            onChanged: (String descValue){
                              description = descValue;
                            },
                          ),
                          Padding(padding: EdgeInsets.all(10)),
                          ElevatedButton(
                              onPressed: _addNote,
                              child: Text("ADD NOTE"))
                        ],
                      ),
                    )
                  ],
                );
              });
        },
      ),
    );
  }
}



class EditDialog extends StatelessWidget {
  const EditDialog({Key? key, required this.notesList, required this.note, required this.updateList}) : super(key: key);
  final TestNote note;
  final List<TestNote> notesList;
  final Function updateList;

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Center(child: Text("Add Dialog")),
      children: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              TextFormField(
                initialValue: note.title,
                decoration: InputDecoration(
                    labelText: "Title",
                    isDense: true,
                    border: OutlineInputBorder()),
                onChanged: (String titleValue){
                  note.title = titleValue;
                },
              ),
              Padding(padding: EdgeInsets.all(10)),
              TextFormField(
                initialValue: note.description,
                decoration: InputDecoration(
                    labelText: "Description",
                    isDense: true,
                    border: OutlineInputBorder()),
                onChanged: (String descValue){
                  note.description = descValue;
                },
              ),
              Padding(padding: EdgeInsets.all(10)),
              ElevatedButton(
                  onPressed: (){
                    updateList(notesList);
                  },
                  child: Text("ADD NOTE"))
            ],
          ),
        )
      ],
    );
  }
}

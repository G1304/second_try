import 'package:flutter/material.dart';
import 'package:second_try/widgets/appbar/appbar.dart';

class MainMenu extends StatefulWidget {
  const MainMenu({Key? key}) : super(key: key);

  @override
  _MainMenuState createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[600],
      appBar: DefAppBar("Main Menu"),
      body: SafeArea(
          child: Center(
        child: ListView(
          padding: EdgeInsets.only(left: 80, right: 80),
          shrinkWrap: true,
          children: [
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/todos');
              },
              child: Text("TODO LIST"),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/fetchcomments');
              },
              child: Text("FETCH COMMENTS"),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/fetchphotos');
              },
              child: Text("Fetch Photos"),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/notes');
              },
              child: Text("Notes"),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),



            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/notestest');
              },
              child: Text("NotesTest"),
              style: ElevatedButton.styleFrom(
                primary: Colors.redAccent,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                padding: EdgeInsets.only(top: 20, bottom: 20),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 20),
            ),

          ],
        ),
      )),
    );
  }
}

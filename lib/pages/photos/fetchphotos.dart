import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:second_try/services/api.dart';
import 'package:second_try/pages/photos/photo_detailed.dart';
import 'package:second_try/widgets/appbar/appbar.dart';

class Photo {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  Photo(
      {required this.albumId,
      required this.id,
      required this.title,
      required this.url,
      required this.thumbnailUrl});

  factory Photo.fromJson(Map<String, dynamic> json) {
    return Photo(
      albumId: json['albumId'],
      id: json['id'],
      title: json['title'],
      url: json['url'],
      thumbnailUrl: json['thumbnailUrl'],
    );
  }
}

class FetchPhotos extends StatefulWidget {
  const FetchPhotos({Key? key}) : super(key: key);

  @override
  _FetchPhotosState createState() => _FetchPhotosState();
}

class _FetchPhotosState extends State<FetchPhotos> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[900],
        appBar: DefAppBar("Fetched Photos"),
        body: FutureBuilder<List<Photo>>(
          future: fetchPhotos(http.Client()),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return PhotoList(photos: snapshot.data!);
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ));
  }
}

class PhotoList extends StatelessWidget {
  const PhotoList({Key? key, required this.photos}) : super(key: key);
  final List<Photo> photos;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        gridDelegate:
            const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
        itemBuilder: (context, index) {
          return GestureDetector(
            child: GridTile(
              footer: Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: Text(
                  "Click Me",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white),
                ),
              ),
              child: Padding(
                padding: const EdgeInsets.all(5),
                child: Column(
                  children: [
                    Image(image: NetworkImage('${photos[index].url}')),
                  ],
                ),
              ),
            ),
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => PhotoDetailed(photo: photos[index]),
                  ));
            },
          );
        });
  }
}

import 'package:flutter/material.dart';
import 'package:second_try/pages/photos/fetchphotos.dart';
import 'package:second_try/widgets/appbar/appbar.dart';



class PhotoDetailed extends StatelessWidget {
  const PhotoDetailed({Key? key, required this.photo}) : super(key: key);
  final Photo photo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[700],
      appBar: DefAppBar("${photo.title}"),
      body: Padding(
        padding: const EdgeInsets.only(left: 40, right: 40),
        child: Center(
          child: Text('Title:\n${photo.title}'
              '\n\nThumbnail URL: ${photo.thumbnailUrl}'
              '\n\nUrl: ${photo.url}'
              '\n\nAlbum ID: ${photo.albumId}'
              '\n\nID: ${photo.id}', textAlign: TextAlign.center, style: TextStyle(
            fontSize: 20,
            color: Colors.white,
            fontFamily: "Monospace",
          ),),
        ),
      )
    );
  }
}
